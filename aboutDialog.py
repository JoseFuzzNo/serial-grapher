from config import *
from PyQt4.QtGui import *
from PyQt4.QtCore import *

class AboutDialog (QWidget):
    def __init__(self):
        super(QWidget, self).__init__()
        self.initUI()
        self.connect()
        
    def initUI (self):
        self.setWindowTitle("About...")
        self.setMinimumSize(QSize(400, 200))
        self.setWindowIcon(QIcon(ICON_PATH + 'graph-icon.png'))
        
        mainLayout = QVBoxLayout()
        self.setLayout(mainLayout)
        
        # Title
        title = QLabel("Serial Grapher v" + str(VERSION))
        title.setAlignment(Qt.AlignCenter)
        title.setStyleSheet("font-weight: bold; font-size: 15px;")
        # Body
        logo = QLabel()
        logo.setPixmap(QPixmap(ICON_PATH + "graph-icon_128.png"))
        logo.setSizePolicy(QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum))
        bodyText = QLabel(u"Copyright (C) 2012 by Jose Carlos Granja Vazquez\n\
More software on https://bitbucket.org/JoseFuzzNo")
        bodyText.setTextInteractionFlags(Qt.TextSelectableByMouse)
        bodyLayout = QHBoxLayout()
        bodyLayout.addWidget(logo)
        bodyLayout.addWidget(bodyText)
        
        # Button layout
        self.closeButton = QPushButton("Close")
        self.closeButton.setIcon(QIcon(ICON_PATH + "close.png"))
        leftSpacer = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)
        rightSpacer = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)
        buttonLayout = QHBoxLayout()
        buttonLayout.addItem(leftSpacer)
        buttonLayout.addWidget(self.closeButton)
        buttonLayout.addItem(rightSpacer)
        
        mainLayout.addWidget(title)
        mainLayout.addLayout(bodyLayout)
        mainLayout.addLayout(buttonLayout)
        
        
        
    def connect (self):
        self.closeButton.clicked.connect(self.hide)