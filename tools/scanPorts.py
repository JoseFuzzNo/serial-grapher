#! /usr/bin/python

import os
import serial

def scanPorts():
    available = []
    for i in os.listdir("/dev/"):
        try:
            s = serial.Serial("/dev/"+i)
            available.append(s.portstr)
            s.close()
        except serial.SerialException:
            pass
    return available

if __name__=='__main__':
    print "Found ports:"
    for n,s in scanPorts():
        print "(%s) %s" % (n,s)

