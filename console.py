from PyQt4.QtGui import *
from PyQt4.QtCore import *

class Console(QTextEdit):
    def __init__(self):
        super(QTextEdit, self).__init__()
        self.initUI()
        
        
    def initUI (self):
        self.setMinimumHeight(100)
        self.setReadOnly(True)
        self.hide()
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        #self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setTextInteractionFlags(Qt.TextSelectableByMouse)
        
        self.setFont(QFont("sans", 8))
        #self.setStyleSheet("background-color: #024; color: #CEF")
        
        
    def toggleVisibility (self):
        if self.isHidden():
            self.show()
        else:
            self.hide()
            
    def mousePressEvent(self, *args, **kwargs):
        pass
    
  