from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.Qwt5 import *
from config import *

class SerialPlot (QwtPlot):
    def __init__(self):
        super(QwtPlot, self).__init__()
        
        self.setMinimumSize(QSize(400, 200))
        self.setAxisScale (2, 0, BUFF_SIZE)
        #self.changeScale()
        self.enableAxis(2, False)
        self.enableAxis(0, True)
        self.curve = QwtPlotCurve()
        self.curve.setRenderHint(QwtPlotItem.RenderAntialiased, True)
        self.useSpline(False)

        self.curve.attach(self)
        
    def changeScale (self):
        self.setAxisScale (0, 0, pow(2,BYTES*8))
        
    def plotData (self, data):
        self.curve.setData(range(BUFF_SIZE), data)
        self.replot()

    def useSpline (self, state):
        if state == Qt.Checked:
            myCurveFitter = QwtSplineCurveFitter()
            myCurveFitter.setFitMode(QwtSplineCurveFitter.Spline)
            self.curve.setCurveAttribute(QwtPlotCurve.Fitted)
            self.curve.setCurveFitter(myCurveFitter)
        else:
            self.curve.detach()
            del self.curve
            self.curve = QwtPlotCurve()
            self.curve.setRenderHint(QwtPlotItem.RenderAntialiased, True)
            self.curve.attach(self)
            myCurveFitter = QwtSplineCurveFitter()
            myCurveFitter.setFitMode(QwtSplineCurveFitter.Auto)
            self.curve.setCurveAttribute(QwtPlotCurve.Inverted)
            self.curve.setCurveFitter(myCurveFitter)
        self.replot()
