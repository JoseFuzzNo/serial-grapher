from serial import Serial
from tools.scanPorts import scanPorts
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from config import *
from serialPlot import SerialPlot
from settingsDialog import SettingsDialog
from aboutDialog import AboutDialog
from console import Console

class MainWindow (QWidget):
    
    def __init__ (self):
        super(QWidget, self).__init__()

        self.port = Serial() 
        self.portList = []
        self.settingsDialog = SettingsDialog()      
        self.aboutDialog = AboutDialog()
        self.dataArray = [0] * BUFF_SIZE
        
        self.initUI()
        self.setTimer()
        self.connect()
        self.refreshDevices()
        
    def closeEvent(self, *args, **kwargs):
        self.settingsDialog.close()
        self.aboutDialog.close()
        return QWidget.closeEvent(self, *args, **kwargs)
    
    def initUI (self):
        # WINDOW
        self.setGeometry (0, 0, 700, 250)
        self.setSizePolicy(QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum))
        self.setWindowTitle ("Serial Grapher")
        self.setWindowIcon(QIcon(ICON_PATH + "graph-icon.png"))

        # LAYOUTS
        mainLayout = QVBoxLayout()
        topBar = QHBoxLayout()
        bottomBar = QHBoxLayout()
        
        # SPACERS
        topSpacer = QSpacerItem(0, 0, QSizePolicy.Expanding ,  QSizePolicy.Fixed)
      
        # TOP-BAR
        deviceLabel = QLabel("Port:")
        self.deviceSelector = QComboBox(self)
        self.byteLabel = QLabel("N bytes:")
        self.byteLengthSelector = QSpinBox()
        self.byteLengthSelector.setMinimum(1)
        self.byteLengthSelector.setValue(BYTES)
        self.setInputType(INPUT_MODE)  
        self.valueIndicator = QLabel("Value: 0")
        self.refreshButton = QPushButton (self)
        self.refreshButton.setIcon(QIcon(ICON_PATH + "refresh.png"))
        self.saveButton = QPushButton (self)
        self.saveButton.setIcon(QIcon(ICON_PATH + "save.png"))
        self.consoleButton = QPushButton (self)
        self.consoleButton.setIcon(QIcon(ICON_PATH + "console.png"))
        self.aboutButton = QPushButton (self)
        self.aboutButton.setIcon(QIcon(ICON_PATH + "about.png"))
        self.configButton = QPushButton (self)
        self.configButton.setIcon(QIcon(ICON_PATH + "config.png"))

        # PLOT
        self.plot = SerialPlot()
        
       
        # CONSOLE
        self.console = Console()
        

        # BOTTOM-BAR
        self.sendLine = QLineEdit()
        self.sendButton = QPushButton(self)
        self.sendButton.setIcon(QIcon(ICON_PATH + "send.png"))
       
        # LAYOUT ARRANGEMENT
        topBar.addWidget(deviceLabel)
        topBar.addWidget(self.deviceSelector)
        topBar.addWidget(self.byteLabel)
        topBar.addWidget(self.byteLengthSelector)
        topBar.addWidget(self.valueIndicator)
        topBar.addItem(topSpacer)
        topBar.addWidget(self.refreshButton)
        topBar.addWidget(self.saveButton)
        topBar.addWidget(self.consoleButton)
        topBar.addWidget(self.aboutButton)
        topBar.addWidget(self.configButton)
        
        bottomBar.addWidget(self.sendLine)
        bottomBar.addWidget(self.sendButton)
        
        mainLayout.addLayout(topBar)
        mainLayout.addWidget(self.plot)
        mainLayout.addWidget(self.console)
        mainLayout.addLayout(bottomBar)
       
        self.setLayout(mainLayout)
        self.show()

    def setTimer (self):
        self.timer = QTimer(self)
        self.timer.start(CLOCK)
    
    def connect (self):
        # Timer
        self.timer.timeout.connect(self.getData)
        
        # Main Window
        self.deviceSelector.currentIndexChanged.connect(self.changePort)
        self.refreshButton.clicked.connect(self.refreshDevices)
        self.configButton.clicked.connect(self.settingsDialog.show)
        self.aboutButton.clicked.connect(self.aboutDialog.show)
        self.sendButton.clicked.connect(self.sendData)
        self.sendLine.returnPressed.connect(self.sendData)
        self.byteLengthSelector.valueChanged.connect(self.setBytesToRead)
        self.consoleButton.clicked.connect(self.console.toggleVisibility)
        self.saveButton.clicked.connect(self.save)
        #self.saveDialog.fileSelected.connect(self.save)

        # Settings Dialog
        self.settingsDialog.splineCheck.stateChanged.connect(self.plot.useSpline)
        self.settingsDialog.rawCheck.stateChanged.connect(self.setInputType)
        
    def changePort (self):
        
        # Si el puerto esta abierto, se cierra
        if self.port.isOpen():
            self.port.close()
            
        # Se abre el nuevo puerto
        portN = self.portList[self.deviceSelector.currentIndex()]
        self.port.setPort(portN)
        self.port.setBaudrate(BAUDRATE)
        self.port.open()
        self.valueIndicator.setText("Value: 0")

        # Se limpia el array de datos y la grafica
        self.dataArray = [0] * BUFF_SIZE
        self.plot.plotData(self.dataArray)
   
        
    def refreshDevices (self):
        for i in range(self.deviceSelector.count()):
            self.deviceSelector.removeItem(0)
        self.portList = []
        self.portList = scanPorts()
        for portName in self.portList:
            self.deviceSelector.addItem(portName)
          
        if self.deviceSelector.count == 0:
            self.deviceSelector.addItem("--");
       
        self.changePort()
     

    def getData (self):
        try:
            if self.port.inWaiting():
                if (INPUT_MODE == RAW):
                    raw = ""
                    for i in range(BYTES):
                        raw += self.port.read()
                    value = int(raw.encode('hex'), 16)
                    
                else:
                    raw = self.port.readline()[:-1]
                    
                    value = 0
                    if float(raw) == int(float(raw)):
                        value = int(float(raw))
                    else:
                        value = float(raw)
                self.console.append(str(value))
                self.valueIndicator.setText("Value: " + str(value))
                self.dataArray = self.dataArray[1:]
                self.dataArray.append(value)
                self.plot.plotData(self.dataArray)
                
        except ValueError:
            self.valueIndicator.setText(("Unrecognized format"))
        except IOError:
            self.refreshDevices()
      
    def sendData (self):
        self.port.write(self.sendLine.text())
        self.sendLine.clear()
    
    def setBytesToRead(self, b):
        global BYTES
        BYTES = b

    def setInputType(self, tp):
        global INPUT_MODE
        if tp:
            INPUT_MODE = RAW
            self.byteLengthSelector.show()
            self.byteLabel.show()
        else:
            INPUT_MODE = CHAR
            self.byteLengthSelector.hide()
            self.byteLabel.hide()
            
    def save (self):
        fileName = QFileDialog.getSaveFileName(self, "Save File",
               "./","Text files (*.txt)")
        outputFile = open (fileName, "w")
        outputFile.write(self.console.toPlainText())
        outputFile.close()