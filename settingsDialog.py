from PyQt4.QtCore import *
from PyQt4.QtGui import *
from serialPlot import *
from collections import namedtuple


class SettingsDialog (QWidget):
    def __init__(self):
        super (QWidget, self).__init__()

        self.initUI()
        self.connect()

    def initUI (self):
        self.setWindowTitle ("Settings")
        self.setWindowIcon(QIcon(ICON_PATH + 'graph-icon.png'))
       
        self.closeButton = QPushButton("Close")
        self.closeButton.setIcon(QIcon(ICON_PATH + "close.png"))
        spacer = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)
        buttonLayout = QHBoxLayout()
        buttonLayout.addItem(spacer)
        buttonLayout.addWidget(self.closeButton)
        
        boxLayout = QHBoxLayout()
        boxLayout.addWidget (self.createGraphicBox())
        boxLayout.addWidget (self.createInputBox())
        
        mainLayout = QVBoxLayout()
        self.setLayout (mainLayout)
        mainLayout.addLayout(boxLayout)
        mainLayout.addLayout(buttonLayout)

    def createGraphicBox (self):
        graphicBox = QGroupBox("Graphics")
        vBox = QVBoxLayout()
        self.splineCheck = QCheckBox("Use spline")
        self.splineCheck.setChecked(False)
        vBox.addWidget (self.splineCheck)
        graphicBox.setLayout(vBox)
        return graphicBox
    
    def createInputBox (self):
        inputBox = QGroupBox ("Input")
        vBox = QVBoxLayout()
        self.rawCheck = QCheckBox("Raw data")
        self.rawCheck.setChecked(INPUT_MODE)
        vBox.addWidget (self.rawCheck)
        inputBox.setLayout(vBox)
        return inputBox

    def connect (self):
        self.closeButton.clicked.connect(self.hide)
